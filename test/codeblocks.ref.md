Don't touch code-blocks outside of lists #fix/emptyline

```yaml
users:
- name: me
  id: 123456
```

- within a list
  we want to ignore contents of (fenced) code blocks, but re-indent them as a whole #fix/emptyline #fix/indent

    ```bash
    - leave
     - this
        - unchanged
    ```

