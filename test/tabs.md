---
title: Markdown Test File - Tabs
---

Tabs suck, don't use them. Some examples how to remove them:

- using `sed` #fix/tab

	```bash
	sed -i 's/\t/$(printf "$%{MD_IND:=4}s")/g' "${fixfiles[@]}"
	```

- using `markdownlint-cli` #fix/tab

	```bash
	markdownlint -f
	```

	with options preserving tabs in code blocks (:warning: also keeps tabs on
	the same line, outside cb., e.g. indentation of the block itself)

	```yaml
	MD010:
	  # Include code blocks
	  code_blocks: false
	  # Fenced code languages to ignore
	  ignore_code_languages: []
	  # Number of spaces for each hard tab
	  spaces_per_tab: *md_ind
	```

However, some programs require them, e.g. in config files. In the rare case
someone pasted such content in a code block, we don't want to touch its
contents. #nofix/tab

```ini
[protagonist]
	character = "John McClane"
	actor = "Bruce Willis"
	trivia = "A Bruce Willis movie, not a Christmas movie"
```

Lastly, it would be nice to preserve tabs in code blocks, but still replace the
indentation of the code block itself, with spaces.

- previous example: replace indentation tabs with spaces, keep tabs in code
  block

	```ini
    [protagonist]
    	character = "John McClane"
    	actor = "Bruce Willis"
    	trivia = "A Bruce Willis movie, not a Christmas movie"
	```
