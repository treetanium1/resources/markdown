Don't touch code-blocks outside of lists #fix/emptyline
```yaml
users:
- name: me
  id: 123456
```

- Within a list:
   We want to ignore contents of (fenced) code blocks, but re-indent them as a whole. MkDocs expects this to be aligned to the next indentation level like with list items. #fix/emptyline #fix/indent
   ```bash
   - leave
    - this
       - unchanged
   ```

