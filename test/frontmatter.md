---
# For personal reasons I require at least a title in the front matter
# #fix/fm/missing. If absent, the file name is used with camel case.
#
# block-scalar strings require indentation #fix/indent #fix/fm
desc-short: |
Notorious Markdown formatting issues to test tools fixing problems such as linters and syntax checkers.
# Obsidian for example accepts tags separated by spaces, but MkDocs requires an array #fix/format #fix/fm
tags: writing/markdown markdown markdown/syntax
# Certain strings should be quoted as some YAML/Markdown processors don't handle characters such as a colon well
url: https://example.com
# Empty keys can be a problem, too. Set value to empty string.
author:
---

# Title

Code block: keep as is, errors and all

```yaml
---
bs: |
no indent
---
```
