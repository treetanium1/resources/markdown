#!/usr/bin/env bash

function t_markdown () {
    UTILDIR="../.."
    . "${UTILDIR}/bash/formatting.sh"
    MDL_CFG=.markdownlint.yaml # NOTE: file outside this dir error'd :shrug:
    . ../markdown.sh

    parse_options "$@"
    shift $((OPTIND - 1))
    local verbose=$VERBOSE
    local -a errors args=("$@")

    [[ -z "$*" ]] && mapfile -t args < \
        <(find . -name "*.md" -printf "%P\n" | grep -v -e .ref.md -e .out.md)

    md_fix -e ".out.md" "${args[@]}"

    for f in "${args[@]}"; do
        diff "${f%.md}.out.md" "${f%.md}.ref.md" 1>"${f%.md}.patch" 2>/dev/null
        if [[ $? -ne 0 || -s "${f%.md}.patch" ]]; then
            errors+=("$f");
            "$verbose" && echo "  ${F_RED}! error${F_RESET} in ${f}"
            # [[ "$VERBOSE" == 'true' ]] && echo "  ${F_RED}!${F_RESET} error in ${f}"
        else
            rm "${f%.md}.patch"
            [[ "$verbose" == 'true' ]] && echo "  ${F_GREEN}* passed${F_RESET} ${f}"
        fi
    done

    echo
    if [[ "${#errors[@]}" -gt 0 ]]; then
        echo "${F_BRED}✗ Failed tests (${#errors[@]}):${F_RESET}"
        printf "  %s\n" "${errors[@]}"
    else
        echo "${F_GREEN}✓ All tests passed${F_RESET}"
    fi
}

if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    t_markdown "$@"

    cleanup
fi
