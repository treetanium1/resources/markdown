---
title: Markdown Test Lists
---

MkDocs
- requires empty lines before lists #fix/emptyline
 - won't render lists properly if they're underindented #fix/indent
      - shift over-indented lines left #fix/indent

markdownlint-cli (command `markdownlint -f`)

- removes indentation for under-indented lines #level0
 + although one might assume the author intended a deeper nesting #level1 #fix/indent
    * the common characters for unordered lists in Markdown are `-+*`, but
      linters might complain about mixing them. Some editors cycle through
      these for different levels. #level2 #fix/indent

      We want to move content within lists along with the respective indent level.

   Back to fewer indentation, assume #level1, having 3 spaces indentation (1 plus 2 for `-&blank;`).

