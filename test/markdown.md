---
title: Markdown Test File
desc-short: |
  Notorious Markdown formatting issues to test tools fixing problems such as linters and syntax checkers.
tags:
  - writing/markdown
  - markdown
  - markdown/syntax
---

MkDocs

- requires empty lines before lists #fix
    - won't render lists properly if they're underindented #fix
        - shift over-indented lines left #fix

markdownlint

- doesn't
    - fix underintended list items correctly #fix
        - instead of indenting more, it removes indentation
        + although it should be clear the author intended a deeper nesting
        * most Markdown spec.s also allow for an asterisk or `+` to indicate list items

            We want to move content within lists along with the respective indent level.

            We want to ignore contents of (fenced) code blocks, but re-indent them as a whole.
            Assume this is on level 1 again, having 3 spaces indentation (1 plus 2 for `- `).

            ```bash
            - leave
             - this
                - unchanged
            ```

Don't touch code-blocks outside of lists

```yaml
users:
- name: me
  id: 123456
```

Some more examples from my personal hell

- ![](img.svg) img
    - local file (code block is under-indented, should have 2 spaces for `-&blank;` #fix

        ```markdown
        ![](img.svg)
        ```

    - online file #fix

        ```markdown
        ![](https://example.com/img.svg)
        ```
