# Title
Expecting empty lines around headings. #fix/emptyline

# Second Top-level Heading
<!-- #complain -->

Only one top-level heading (h1) seems to be a common convention. Can't really do anything about this, only the user knows whether it is fine to change all h1 headings to h2. But the linter should complain about this.


## Multiple Empty Lines

I prefer this for clearer reading as there are a lot of single empty lines.
A double empty line makes it easier to differentiate sections.
See markdownlint config rule `MD012`, `maximum: 2`. However, this allows double empty lines everywhere.
