## To Do

Most of these issues should be fixed by sourcing `markdown.sh` and running `md_fix`. However, there are some major things not yet handled properly.

:warning: Still some major issues open. I'm close to giving up on some of the minor issues, this has already cost too much time.

- [ ] Markdown issues
    - [x] skip symlinks
    - [x] YAML front matter
        - [x] **invalid YAML** :warning: MkDocs fails
            - [x] detected with yamllint (has no fix functionality)
            - [x] incorrect indentation for block scalars (multi-line string blocks)
            - [x] fails when batch-processing
        - [ ] fix over-indentation array entries
        - [x] ensure fm exists
        - [x] ensure fm at least contains `title:` (generated from file name if missing)
            - [x] camel case
        - [x] format for `tags:` - MkDocs requires array
    - [ ] Markdown body
        - [x] replace tabs by spaces
        - [x] use `markdownlint -f` to fix problems as far as possible
        - [ ] try other linters/formatters instead of `markdownlint-cli`
            - [ ] [markdownlint-cli2]
            - [ ] [mdformat]
        - [ ] manual fixes for cases not covered (correctly) by markdownlint-cli
            - [ ] lists
                - [x] list indentation
                - [x] **apply unordered list fixes also to ordered lists**
                - [x] don't treat lines starting with emphasised text `*text*` as list items or generally ignore if there's no space after `[-+*]`?
                - [ ] lists starting with level 0 already indented become level 1, instead of removing over-indentation
            - [ ] code blocks
                - [x] MkDocs expects indentation at next level, just like with list items, not aligned to text
                - [x] indented, unfenced code blocks - just don't use them
                - [ ] something removes preceding `#$` in code blocks (it's old-fashioned anyway and leads to poor syntax highlighting, but I'm not that opinionated)
            - [x] stop trailing dots in headings from being removed (Markdownlint rule MD026)
            - [x] tags at line start `^#\S` (interpreted as headings by MkDocs) - now prefixed with `Tags:&blank;`
- [ ] features
    - [ ] patterns to combine several options else specified in MDF config file, e.g. `critical` to only fix invalid syntax (especially front matter)
- [ ] scripts/programs
    - [ ] remove external dependencies, e.g. bash script `$UTILDIR/bash/tools.sh` #prio2
    - [x] add option `-o <output pattern/extension>` allowing output to different files (currently in-place)
    - [ ] consider using [getoptions] instead of `getopts`
- [ ] testing
    - [x] create input and reference output files #prio3
        - [x] lists
        - [x] code blocks
        - [x] headings
        - [x] (YAML) front matter
    - [ ] use testing framework #prio3
        - [ ] [ShellSpec](https://shellspec.info/)?
            - [ ] use ShellSpec DSL?
    - [ ] enforce through pre-commit hook 
- [ ] Repository
    - [x] make this usable as a git `pre-commit` hook, e.g. by using the [pre-commit framework[pre-commit]]
        - [x] provide `pre-commit` hook example
        - [x] make it installable: function `git_pre-commit_install` in `git_pre-commit.sh`
    - [ ] documentation
        - [x] `markdownlint` config file `mdl.*`: merging defaults + custom
        - [ ] `mdfix.yml` config file
    - [ ] suggest/contribute some fixing capabilities to `markdownlint-cli(2)`?
    - [ ] create (animated) images, showing what it does

[yq]: <https://mikefarah.gitbook.io/yq/>
[markdownlint-cli2]: <https://github.com/DavidAnson/markdownlint-cli2>
[mdformat]: <https://github.com/executablebooks/mdformat>
[mdformat-frontmatter]: <https://github.com/butler54/mdformat-frontmatter>
[getoptions]: <https://github.com/ko1nksm/getoptions>
[pre-commit]: <https://pre-commit.com/>
