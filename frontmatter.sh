#!/usr/bin/bash

# DESCRIPTION: functions to process and fix the YAML front matter (in Markdown files)
# #markdown/frontmatter

UTILDIR="${UTILDIR:-"$(realpath ./util)"}"
[[ ! -d "$UTILDIR" ]] && { echo "ERROR: can't find utilies \$UTILDIR=${UTILDIR}" >&2; exit 1; }

set -e
. "${UTILDIR}/yq.sh" && ensure_yq
. "${UTILDIR}/bash/tools.sh"
. "${UTILDIR}/markdown/common.sh"
set +e
. "${UTILDIR}/bash/formatting.sh"

function fm_set () {
    # Set key=value in frontmatter of file
    local msg_usage="Usage: ${FUNCNAME[0]} <key> <val> <file>"
    local msg_desc="Set key=value in frontmatter of file"
    [[ $# != 3 ]] && echo "ERROR. ${msg_usage}" && return 2
    local key=${1} value=${2} path=${3}

    # find files where key isn't set to val
    if [[ -f "$path" ]]; then
        fm_ensure "$f"
        yq -f process -i ".${key}=\"${value}\"" "$f"
    else
        echo "ERROR. file/dir not found: ${path}"
    fi
}

function fm_matchFiles () {
    # Find files with front matter key=value
    # #markdown/frontmatter
    local msg_usage="Usage: ${FUNCNAME[0]} [-i] <key> <val> [<path(s)>]"
    local msg_options="\t-i\t\tinvert match\n\t-h, --help\tprint this help"
    local msg_desc="Find files with front matter key=value"
    local msg_help="${msg_usage}\n${msg_desc}\n\n${msg_options}"
    [[ "$1" == "-i" ]] && { invert=true; shift; } || invert=false
    [[ "$1" == "-h" || "$1" == "--help" ]] && { echo -e "$msg_help"; return; }
    [[ $# != 2 && $# != 3 ]] && echo -e "ERROR. ${msg_usage}" && return 2
    local key="$1" val="$2"
    shift 2
    local dir="${*:-.}"

    local -a fixfiles
    mapfile -t fixfiles < <(find_files "*.md" "$dir")
    for f in "${fixfiles[@]}"; do
        if ! $invert && fm_exists "$f"; then
            [[ $(yq -f extract '.'"$key" "$f" 2>/dev/null) == "$val" ]] && files+=("$f")
        else
            if ! fm_exists "$f" || [[ $(yq -f extract '.'"$key" "$f" 2>/dev/null) != "$val" ]]; then
                files+=("$f")
            fi
        fi
    done
}

function fm_exists () {
    # return 0 if frontmatter exists, else 1
    local msg_usage="Usage: ${FUNCNAME[0]} <file>"
    local msg_desc="return 0 if frontmatter exists, else 1"
    # TODO: allow frontmatter after first line?
    awk 'NR==1 && ! /^---/ {exit 1}' "$1"
}

function fm_ensure () {
    # Create empty frontmatter if absent
    local msg_desc="Create empty frontmatter if absent"
    fm_exists "$1" || sed -i '1i\---\n---' "$1"
}

function fm_get () {
    # Get value of key in frontmatter of file. Error with 1 if frontmatter or key are absent.
    local msg_desc="Get value of key in frontmatter of file. Error with 1 if frontmatter or key are absent."
    local msg_usage="Usage: ${FUNCNAME[0]} <file> <key>"
    fm_exists "$1" && yq -e=1 -f extract ".${2}" "$1" 2>/dev/null
}

function fm_ensureTitle () {
    # If title is missing, use file name
    local msg_desc="If title is missing, use file name"
    local msg_usage="Usage: ${FUNCNAME[0]} <file>"
    local title

    if ! fm_exists "$1" || ! fm_get "$1" title >/dev/null; then
        fm_ensure "$1"
        title="$(basename "${1}")"
        title="${title%%.*}"
        title="${title//_/ }"
        title="$(camel_case "$title")"
        [[ "$VERBOSE" == 'true' ]] && echo "** setting title: ${title}"
        yq -f process -i ".title=\"${title}\"" "$1"
    fi
}

function fm_fix_tags_format () {
    # fix format of frontmatter tags: use array
    # Explanation: Obsidian for example accepts tags as space-separated string, others (e.g. MkDocs)
    # require an array
    # #markdown/frontmatter
    local msg_usage="Usage: ${FUNCNAME[0]} <file>"
    grep -qi 'tags:\s\+[a-z]' "$1" && sed -i '/^tags:/s/\s\+/\n  - /g' "$1"
}

function fm_fix_unquoted () {
    # Decription:
    #     Quote problematic string values: empty, colon, ...
    # Motivation:
    #     Some programs choke on parsing the fm if strings contain certain characters and aren't
    #     quoted.
    # Tags: #markdown #markdown/frontmatter
    local msg_usage="Usage: ${FUNCNAME[0]} <file>"
    local msg_desc="Quote problematic string values: empty, colon, ..."
    local fm

    if fm_exists "$1"; then
        fm="$(yq -f extract "$1" 2>/dev/null)" || \
            {
                echo "ERROR: invalid YAML in '${1}', failed to process" >&2
                return 1
            }
        # colon in string -> quote
        # TODO: Skip YAML scalars `- |` and `| >`
        grep -q ': .*:' <<< "$fm" && \
            yq -f process -i '(.[] | select(type=="!!str" and test(".*:.*"))) style="double"' "$1"
        # empty key -> empty quote
        grep -q ':\s*$' <<< "$fm" && \
            yq -f process -i '(.[] | select(tag == "!!null")) style="double"' "$1"
        # NOTE: For an explanation of the yq command used here see
        # https://mikefarah.gitbook.io/yq/how-it-works#complex-assignment-operator-precedence-rules
    fi
}

function fm_fix_YAMLScalars () {
    # Fix files with YAML block/folded scalars without indentation of the key's value, e.g.
    # key: |
    # value  # should be indented
    # Why? For some unknown reason performing all Markdown fixes on a whole directory, as opposed
    # to single files, removes the indentation in YAML block/folded scalars.
    # What are block/folded scalars? Extended strings, see https://yaml-multiline.info/

    parse_options
    shift $((OPTIND-1))

    [[ -z "$*" ]] && return

    local -a allfiles fixfiles
    # find files with YAML scalar: either `: |` or `: >`
    mapfile -t allfiles < <(egrep_files "*.md" ":\s*[|>]" "$@")
    [[ -z "${allfiles[*]}" ]] && return

    for f in "${allfiles[@]}"; do
        fm_exists "$f" && \
            { sed -n '/^---/,/^---/p' "$f" | yamllint - &>/dev/null || \
              fixfiles+=("$f"); }
    done

    if [[ -n "${fixfiles[*]}" ]];  then
        tmp_awk="$(mktemp)"
        # NOTE: processing one file at a time as passing all files to awk resulted in errors
        for f in "${fixfiles[@]}"; do
            if [[ -n "$SUFFIX" ]]; then
                f_out="${f%.*}${SUFFIX}"
            elif [[ -n "$OUTPUT" ]]; then
                f_out="$OUTPUT"
            else
                f_out="$f"
            fi
            [[ ! -f "$f_out" ]] && cp "$f" "$f_out"
            awk -v indent="${YAML_IND:-2}" \
                -f "$UTILDIR/markdown/fix_indent_fm.awk" \
                "$f_out" > "$tmp_awk"
                # NOTE: option '-i inplace' is version-dependent and not used for portability
                # -i inplace \
            cp "$tmp_awk" "$f_out"
        done
        rm "$tmp_awk"
    fi
}

function fm_fix () {
    # Fix YAML frontmatter (in Markdown files)
    local msg_desc="Fix YAML frontmatter (in Markdown files)"
    local msg_usage="Usage: ${FUNCNAME[0]} <path(s)>"
    local msg_options="Options:\n\t-c\tcreate fm if absent"
    
    parse_options "$@"
    shift $((OPTIND-1))
    local -a paths fixfiles _fixfiles errors criterrors
    [[ -n "$*" ]] && paths=("$@") || paths=(".")
    # collect files from path arg.s
    mapfile -t fixfiles < <(find_files "*.md" "${paths[@]}")

    # sort out files without front matter
    for f in "${fixfiles[@]}"; do
        fm_exists "$f" && _fixfiles+=("$f")
    done
    fixfiles=("${_fixfiles[@]}")

    echo "Fixing YAML front matter"

    echo "* fixing YAML block/folded scalar indentation"
    fm_fix_YAMLScalars "${paths[@]}"

    echo "* fixing unquoted (empty) values, tags format and ensuring title"
    for f in "${fixfiles[@]}"; do
        [[ "$VERBOSE" == 'true' ]] && echo "  * file '${f}'"

        if [[ -n "$SUFFIX" ]]; then
            f_out="${f%.*}${SUFFIX}"
        elif [[ -n "$SUFFIX" ]]; then
            f_out="$OUTPUT"
        else
            f_out="$f"
        fi

        [[ ! -f "$f_out" ]] && cp "$f" "$f_out"
        [[ "$CREATE" == "true" ]] && fm_ensure "$f_out"
        fm_fix_unquoted "$f_out"
        fm_fix_tags_format "$f_out"
        fm_ensureTitle "$f_out"
    done

    echo "* linting YAML front matter"
    for f in "${fixfiles[@]}"; do
        # TODO:
        # - try different YAML (fm) linter

        # check whether fm can even be processed
        if yq -f extract "$f" &>/dev/null; then
            yq -f extract "$f" 2>/dev/null | yamllint - &>/dev/null || \
                errors+=("$f")
        else
            criterrors+=("$f")
        fi
    done

    if [[ -n "${criterrors[*]}" ]]; then
        echo "${F_ERROR}Invalid YAML front matter${F_RESET} (${#criterrors[@]})"
        [[ "$VERBOSE" == 'true' ]] && printf "%s\n" "${criterrors[@]}"
        return 1
    elif [[ -n "${errors[*]}" ]]; then
        echo "${F_WARN}Formatting issues in YAML front matter${F_RESET} (${#errors[@]})"
        [[ "$VERBOSE" == 'true' ]] && printf "%s\n" "${errors[@]}" || true
    else
        echo "${F_GOOD}All YAML front matter valid${F_RESET}"
    fi
}

function fmmatch_push () {
    # Push all files where YAML frontmatter <key> has <value> to <target dir>
    # #markdown #markdown/frontmatter
    local msg_usage="Usage: ${FUNCNAME[0]} <key> <val> [<source dir>] <target dir>"
    [[ $# != 3 && $# != 4 ]] && echo "$msg_usage" && return 2
    local key=${1:-visibility} value=${2:-public}
    [[ $# == 4 ]] && { source_dir="$3"; shift; } || source_dir='.'
    local -a syncfiles

    mapfile -t syncfiles < <(fm_matchFiles "$key" "$value" "$source_dir")
    # additional files from file
    # TODO: find a better solution
    if [[ -f includes.txt ]]; then
        while read -r l; do
            syncfiles+=("$l")
        done < includes.txt
    fi
    # sort and remove duplicates (ensure unique)
    mapfile -t syncfiles < <(printf '%s\n' "${syncfiles[@]}" | sort -u)

    echo "Copying ${#syncfiles[@]} files to $3 ..."
    rsync -aLR "${syncfiles[@]}" "$3"

    echo "Done."
}

function fmmatch_pull () {
    [[ $# != 1 ]] && echo "Usage: <source dir>" && return 2

    echo "Copying files from $1 ..."
    cp -ar "$1"/* .
    # rsync -aLR "$3/*" .

    echo "Done."
}

if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    fm_fix "$@"
fi
