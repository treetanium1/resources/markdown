#!/usr/bin/bash
#
# Description:
#
# Author:
#     Sebastian Erfort
#     https://sebastianerfort.github.io
#
# Keywords / Tags: #markdown #linting #obsidian #mkdocs

UTILDIR="${UTILDIR:-"$(realpath ./util)"}"
[[ ! -d "$UTILDIR" ]] && { echo "ERROR: can't find utilies \$UTILDIR=${UTILDIR}"; exit 1; }

set -e
. "${UTILDIR}/yq.sh" && ensure_yq
. "${UTILDIR}/bash/tools.sh"
. "${UTILDIR}/markdown/common.sh"
. "${UTILDIR}/markdown/frontmatter.sh"
. "${UTILDIR}/markdown/markdownlint.sh"
set +e
. "${UTILDIR}/bash/formatting.sh"

# set markdownlint config file
MDL_CFG="${MDL_CFG:-$(mdl_find_config)}"
# retrieve Markdown indentation from markdownlint config file, set default if not found
MD_IND=${MD_IND:-"$(mdl_get_indent)"}

function md_fix_listIndent () {
    # Fix indentation of list items
    #
    # Markdown linters with fixing capabilities don't indent under-indented list
    # items properly, indentation is removed instead of added. If the author
    # indented an item more, it should be assumed this is a more nested list
    # item.
    #
    # NOTE: Treat tabs first! Currently they'll only be translated to 1 space in
    # the AWK scripts.

    parse_options "$@"
    # remove options from arguments (see man getopts: examples)
    shift $((OPTIND - 1))

    mapfile -t mdfiles < <(find_files "*.md" "$@")
    echo "* fixing list indentation"

    # NOTE: processing one file at a time as passing all files to awk resulted in errors
    if [[ -z "$SUFFIX" && -z "$OUTPUT" ]]; then # edit files in-place
        tmp_awk="$(mktemp)"
        for f in "${mdfiles[@]}"; do
            # NOTE: option '-i inplace' is version-dependent and not used for portability
            awk -v indent="$MD_IND" -f "${UTILDIR}/markdown/wrong_indent.awk" "$f" \
                > "$tmp_awk"
            cp "$tmp_awk" "$f"
        done
        rm "$tmp_awk"
    else # output to separate file
        for f in "${mdfiles[@]}"; do
            [[ -n "$SUFFIX" ]] && f_out="${f%.*}${SUFFIX}" || f_out="$OUTPUT"
            awk -v indent="$MD_IND" -f "${UTILDIR}/markdown/wrong_indent.awk" "$f" > "$f_out"
        done
    fi

    cleanup
}

function md_fix_tagLineStart () {
    # find lines starting with tag #... and prepend with text as some Markdown parsers
    # interpret this as headings
    local -a fixfiles
    mapfile -t fixfiles < <(egrep_files '*.md' '^#[a-z]' "$@")

    echo "* fixing lines starting with tag: #..."
    for f in "${fixfiles[@]}"; do
        sed -i '/^#[a-zA-Z]/s/./Tags: &/' "$f"
    done
}

function md_fix_noTabs () {
    parse_options "$@"
    # remove options from arguments (see man getopts: examples)
    shift $((OPTIND - 1))

    # TODO: skip code blocks
    local -a fixfiles
    mapfile -t fixfiles < <(find_files "*.md" "$@")
    # find files with tab character using grep with Perl regex
    mapfile -t fixfiles < <(grep -l -P '\t' "${fixfiles[@]}")

    [[ "${#fixfiles[@]}" -gt 0 ]] && echo "* replacing tabs by spaces in ${#fixfiles[@]} file(s) ..."
    if [[ -z "$SUFFIX" ]]; then
        # fix in-place
        sed -i "s/\t/$(printf "%${MD_IND}s")/g" "${fixfiles[@]}"
    else
        for f in "${fixfiles[@]}"; do
            sed "s/\t/$(printf "%${MD_IND}s")/g" "$f" > "${f%.*}${SUFFIX}"
        done
    fi

    # if called directly, not through other function, clean up
    [[ ${#FUNCNAME[@]} -eq 1 ]] && cleanup
}

function md_fix () {
    local -a warn err
    # Prerequisites
    which yq >/dev/null 2>&1 || { echo "ERROR: could not find yq"; return; }

    parse_options "$@"
    if [[ -n "$PATTERN" ]]; then
        [[ -n "$CONFIG" ]] && { echo "WARNING: config ${MDF_CFG} and pattern provided - ignoring config file"; }
        case "$PATTERN" in
            critical) MDF_RULES=".frontmatter"
        esac
    else
        MDF_CFG="${CONFIG:-"$MDF_CFG"}"
        [[ -z "$MDF_CFG" ]] && MDF_CFG="${UTILDIR}/markdown/mdfix.yml"
        [[ ! -f "$MDF_CFG" ]] && { echo "ERROR: config not found '${MDF_CFG}'"; exit 1; }
        # FIX:
        # collect rules with true value from MDF_CFG in MDF_RULES
    fi

    # TODO: for now using markdownlint-cli, see rule MD010 (not handling code
    # blocks well)
    # [[ $(yq "${UTILDIR}/markdown/mdfix.yml" '.mdfix.noTabs') == 'true' ]] && \
    #     md_fix_noTabs "$@"
    if [[ $(yq '.noTabs' "$MDF_CFG") == 'true' ]]; then
        export MDL_RULES="MD010"
        mdl_run "$@" || warn+=("noTabs")
        unset MDL_RULES
    fi

    # front matter fixes, see frontmatter.sh
    # NOTE: most important fixes really, fm errors break MD processors such as
    # Python-Markdown (MkDocs)
    if [[ $(yq '.frontmatter' "$MDF_CFG") == 'true' ]]; then
        fm_fix "$@" || err+=("frontmatter")
    fi

    echo "Fixing Markdown body"

    if [[ $(yq '.listIndent' "$MDF_CFG") == 'true' ]]; then
        md_fix_listIndent "$@" || warn+=("listIndent")
    fi

    if [[ $(yq '.tagLineStart' "$MDF_CFG") == 'true' ]]; then
        md_fix_tagLineStart "$@" || warn+=("tagLineStart")
    fi

    if [[ $(yq '.markdownlint' "$MDF_CFG") == 'true' ]]; then
        mdl_run "$@" || warn+=("markdownlint")
    fi

    if [[ -n "${warn[*]}" ]]; then
        echo "WARNINGS: ${warn[@]}"
    fi
    if [[ -n "${err[*]}" ]]; then
        echo "ERRORS: ${err[@]}"
        return 1
    else
        return 0
    fi
}

# if executed, not sourced
if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    md_fix "$@"

    cleanup
else
    return 0
fi
