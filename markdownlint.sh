#!/usr/bin/bash
#
# Description:
#
# Author:
#     Sebastian Erfort
#     https://sebastianerfort.github.io
#
# Keywords / Tags: #markdown #linting #obsidian #mkdocs

UTILDIR="${UTILDIR:-"$(realpath ./util)"}"
[[ ! -d "$UTILDIR" ]] && { echo "WARNING: can't find utilies \$UTILDIR=${UTILDIR}"; }
. "${UTILDIR}/bash/formatting.sh"

function mdl_get_indent () {
    # retrieve Markdown indentation from markdownlint config file, set default if not found
    [[ -n "$MDL_CFG" ]] && { yq -e=1 '.MD007.indent' "$MDL_CFG" 2>/dev/null | grep "[0-9]\+"; } || \
        echo 4
}

function mdl_find_config () {
    # search markdownlint config file
    # TODO: add common locations
    if [[ -z "$MDL_CFG" ]]; then
        [[ -f .markdownlint.yaml ]] && echo ".markdownlint.yaml"
        [[ -f markdownlint.yaml ]] && echo "markdownlint.yaml"
    fi
}

function mdl_run () {
    # use fix functionality of [markdownlint-cli](https://github.com/igorshubovych/markdownlint-cli)
    # TODO: ensure passing paths to markdownlint works as expected

    parse_options "$@"
    # remove options from arguments (see man getopts: examples)
    shift $((OPTIND - 1))

    if which markdownlint &>/dev/null; then
        echo "* markdownlint fixes ..."
        echo "  Markdownlint: config file ${MDL_CFG}, indent ${MD_IND} spaces"

        local mdl_log=markdownlint.log errors=0
        local mdl_opts=(-f -o "${mdl_log}")
        [[ -n "$MDL_CFG" ]] && mdl_opts+=(-c "${MDL_CFG}")
        [[ -n "$MDL_RULES" ]] && mdl_opts+=(--enable "${MDL_RULES}" --)
        if [[ -n "$SUFFIX" || -n "$OUTPUT" ]]; then
            for f in "$@"; do
                [[ -n "$SUFFIX" ]] && f_out="${f%.*}${SUFFIX}"
                [[ -n "$OUTPUT" ]] && f_out="${OUTPUT}"
                [[ ! -f "$f_out" ]] && cp "$f" "$f_out"
                markdownlint "${mdl_opts[@]}" "$f_out" || ((errors++))
            done
        else # fix in-place
            markdownlint "${mdl_opts[@]}" "$@" || ((errors++))
        fi
        ((errors == 0)) || \
            echo "${F_RED}! ${errors} file(s) with unresolved issues${F_RESET}, see ${mdl_log}"
    else
        echo "WARNING: missing markdownlint CLI, skipping further fixes"
    fi
}
