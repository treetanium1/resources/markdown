#!/bin/bash
# TODO: try [lefthook](https://github.com/evilmartians/lefthook), it would make
# collecting files to be comitted and such easier

function git_pre-commit () {
    . "${UTILDIR}/markdown/markdown.sh"
    toplevel="${GIT_DIR%/.git*}"
    echo "$toplevel"

    local -a files_fix files_fixed
    # default: run against staged files to be committed
    if [[ -z "$*" ]]; then
        mapfile -t files_fix < <(git diff --staged --name-only | grep ".md$")
    # for use e.g. in CI/CD: run against arguments
    else
        files_fix=("$@")
    fi
    [[ -z "${files_fix[*]}" ]] && return

    MDL_CFG="${MDL_CFG:-"${toplevel}/.markdownlint.yaml"}"
    md_fix -v -c "${toplevel}/mdfix.yml" "${files_fix[@]}"
    errror_code=$?

    for f in "${files_fix[@]}"; do
        [[ -n "$(git diff --name-only "$f")" ]] && files_fixed+=("$f")
    done

    if [[ -n "${files_fixed[*]}" ]]; then
        echo "Fixed ${#files_fixed[@]} files:"
        printf "'%s'\n" "${files_fixed[@]}"
        git add -u "${files_fixed[@]}"
    fi
    return $errror_code
}

git_pre-commit_install () {
    local hook="${1}/hooks/pre-commit"
    cat > "$hook" << 'EOF'
#!/bin/bash

echo "Running pre-commit hook"

# assuming calling this from docs/<submodule>/
export UTILDIR="$(realpath ../../util)"
. "${UTILDIR}/markdown/git_pre-commit.sh"
git_pre-commit
EOF

    chmod +x "$hook"
}

if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    if [[ "$1" == '-i' || "$1" == '--install' ]]; then
        shift
        git_pre-commit_install "$1"
    else
        git_pre-commit "$@"
    fi
fi
