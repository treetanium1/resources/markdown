function cleanup () {
    unset OUTPUT SUFFIX VERBOSE CREATE QUIET
    unset print_help
}

function parse_options () {
    function print_help () {
        cat << EOF
Usage: ${FUNCNAME[2]} [OPTION(S)] ... [--] [PATH(S)] ...
Parse program options and inputs.

    -h,--help     print this help
    -e <ext>      output file extension/suffix
    -o <name>     output file name
    -v,--verbose  verbose output
    --clean       reset
    --            end of options, interpret remaining arguments as files
EOF
    }

    # option defaults
    SUFFIX=""
    CREATE=true
    VERBOSE=false
    QUIET=false

    # transform long opt.s to short ones, getopts doesn't parse long options
    for arg in "$@"; do
        shift
        case "$arg" in
            '--help')    set -- "$@" '-h'   ;;
            '--verbose') set -- "$@" '-v'   ;;
            '--quiet')   set -- "$@" '-q'   ;;
            '--clean')   cleanup; return    ;;
            '--config')  set -- "$@" '-c'   ;;
            '--no-create') CREATE=false     ;;
            *)           set -- "$@" "$arg" ;;
        esac
    done

    OPTIND=1 # reset for getopts to work when used repeatedly
    while getopts ":he:o:c:vq-" opt; do # NOTE: leading : suppresses getopts error on unknown option
      case "$opt" in
        h) print_help; cleanup; return;;
        o) OUTPUT="$OPTARG";;
        e) SUFFIX="$OPTARG";;
        c) CONFIG="$OPTARG";;
        v) VERBOSE=true;;
        q) QUIET=true;;
        -) break;; # don't parse anything after --
        :) echo "Option requires an argument: -${OPTARG}"; return 2;;
        ?) echo "Invalid option: -${OPTARG}"; return 2;;
      esac
    done
}
