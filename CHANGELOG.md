# CHANGELOG

2024-01-24: added Git pre-commit hook to run linting fixes/formatting against staged files
2024-03-20: improve Git pre-commit hook and installation of it

