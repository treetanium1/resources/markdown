---
title: Markdown Resources
---

:warning: Disclaimer: This is absolutely a work-in-progress and not usable at the moment before I make further dependencies available. I started this project to fix a large number of files from different authors in a documentation system. Confronted with various limitations in existing linters/style fixers I decided to make this into a re-usable tool in the hope someone might find it useful. Admittedly, I was also looking for something to improve my AWK skills on.

# Markdown Utilities

Tools for

- linting/style checks, using existing tools as far as possible (e.g. `markdownlint-cli`)
- utilities
    - find files where front matter key has certain value
    - pull files from or push files to directory where front matter key has certain value
    - add/update keys (in batch)


## Features

AFAIK there are few [notes-markdown-lint] and they are limited. `markdownlint-cli` for example states for its fixing capabilities

```
-f, --fix   fix basic errors [...]
```

To name some main reasons why I am extending these features

- General
    - `markdownlint-cli` can replace tabs and skip code blocks, but it would be preferable to only skip their content, not the preceding indentation
- Markdown
    - lists
        - under-indented list items are shifted left, decreasing indentation - this doesn't seem right, the author probably intended to increase the indentation level
- (YAML) front matter
    - isn't fixed and ~~crashes~~ used to crash some programs, for example MkDocs
    - no/wrong indentation for [YAML scalar strings][yaml-scalar] (e.g. `longstr: |`)
    - empty keys without value: add empty string or remove
    - compatibility issues, for example keys where different programs expect different value formats


## Config

Different fixes can be toggled through a config file and this file can be specified as a command argument

```bash
md_fix -c mdf.yml <path(s)>
```


## Usage

Add this repo to your project as a Git submodule in `./util/markdown`. Source and run individual functions separately or execute scripts directly with arguments

```bash
. util/markdown/frontmatter.sh && fm_ensureTitle <path(s)>
. util/markdown/markdown.sh && md_fix_tagLineStart <path(s)>
# or directly execute, performing ALL fixes
./util/markdown/markdown.sh <path(s)>
```


### Pre-commit Hook

Install with 


## Dependencies 

### markdownlint

The command line tool `markdownlint-cli` is used to fix many Markdown issues. See above for caveats.

For compatibility between MkDocs, Obsidian and others, as well as because of personal preferences, some changes to the default config have been made

- use 4 spaces indentation instead of 2
- replace tabs by spaces, except in code blocks (preferable would be only in content)

See changes with respect to default settings `mdl_default.yaml` in
`mdl_custom.yaml`. To create a complete config, with custom settings overwriting
defaults, merge these YAML files, e.g. using [yq].[^1]
Then specify the path to your markdownlint config file by setting the environment
variable `MDL_CFG` which is passed to the `markdownlint` CLI tool.

```bash
yq eval-all '. as $item ireduce ({}; . * $item)' .mdl_default.yaml .mdl_custom.yaml > .mdl.yaml
export MDL_CFG=.mdl.yaml
```

[^1]: <https://mikefarah.gitbook.io/yq/operators/multiply-merge>


## References

- [YAML Front Matter Linters][notes-markdown-lint]

[yq]: <https://mikefarah.gitbook.io/yq/>
[markdownlint-cli2]: <https://github.com/DavidAnson/markdownlint-cli2>
[mdformat]: <https://github.com/executablebooks/mdformat>
[mdformat-frontmatter]: <https://github.com/butler54/mdformat-frontmatter>
[yaml-scalar]: <https://yaml-multiline.info/>
[notes-markdown]: <https://sebastianerfort.github.io/notes/seb_pub/tech/writing/Markdown/>
[notes-markdownlint]: <https://sebastianerfort.github.io/notes/seb_pub/tech/software/markdownlint/>
[notes-markdown-lint]: <https://sebastianerfort.github.io/notes/seb_pub/tech/writing/Markdown/#linting-and-style>
