#!/usr/bin/awk
# Description: replace tabs by spaces, except for code block content
# FIX: WIP, do NOT use

BEGIN {
    # defaults if not set as option -v <var>=<val>
    if ( !indent ) { indent = 2 }
    if ( !fix ) { fix = 1 } # fix or complain?
    ind_curr_was = 0; ind_curr_is = 0
    ind_prev_was = 0; ind_prev_is = 0
    lvl = 0; ind_lvl = 0
    fm=0; cb=0; lst=0
}

# front matter (ignore in code block)
/^---/ && cb%2 != 1 { fm++ }
# code blocks
/^\s*```/ { cb++ }

# MAIN
{
    if ( /\t/ ) {
        # beginning of code block: determine cb indent
        if ( /^\s*```/ && cb%2 == 1 ) {
            l_noTab = $0; sub(/\t/, sprintf("%"indent"s", "") ,l)
            l_noInd = l_noTab; sub(/^\s*/,"",l_noInd)
            ind_cb_sp = length($0) - length(l) # indent in spaces
            # TODO: indent in # of tabs
            # ind_cb_tab = ...
        }
        # inside code block
        if ( cb%2 == 1 && ! /^\s*```/ ) {
        }
    } else { print }
